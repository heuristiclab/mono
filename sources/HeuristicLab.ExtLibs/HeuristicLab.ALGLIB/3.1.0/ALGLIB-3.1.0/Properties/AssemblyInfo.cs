﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ALGLIB-3.1.0")]
[assembly: AssemblyDescription("ALGLIB® - numerical analysis library, 1999-2010")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ALGLIB project")]
[assembly: AssemblyProduct("ALGLIB-3.1.0")]
[assembly: AssemblyCopyright("Copyright © Sergey Bochkanov (ALGLIB project) 1999-2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("D68158F2-56A6-41DD-8CBA-FF1153A9CA4A")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.1.0.0")]
[assembly: AssemblyFileVersion("3.1.0.0")]
