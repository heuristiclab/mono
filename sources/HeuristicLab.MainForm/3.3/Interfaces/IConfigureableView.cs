﻿
namespace HeuristicLab.MainForm {
  public interface IConfigureableView : IView {
    void ShowConfiguration();
  }
}
