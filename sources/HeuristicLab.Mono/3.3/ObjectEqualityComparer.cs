using System;
using System.Collections.Generic;

namespace HeuristicLab.Mono {

  [Serializable]
  public class ObjectEqualityComparer<T> : EqualityComparer<T> {
    public override int GetHashCode(T obj) {
      return EqualityComparer<T>.Default.GetHashCode(obj);
    }
    public override bool Equals(T a, T b) {
      return EqualityComparer<T>.Default.Equals(a, b);
    }
  }
}

