using System;
using System.Reflection;
using System.Linq;

namespace HeuristicLab.NUnit.Common {
  public class Accessor {
    public object Target { get; private set; }
    private Type Type { get; set; }

    public Accessor(object target) {
      this.Target = target;
      this.Type = target.GetType();
    }

    // call methods through reflection.
    // this is a workaround and should be replaced
    // by proper classes generated through reflection.emit
    public object Call(string method, params object[] args) {
      Type[] types = (from a in args select a.GetType()).ToArray();
      MethodInfo m = this.Type.GetMethod(method,
        BindingFlags.Instance |
        BindingFlags.Public | BindingFlags.NonPublic,
        null, types, null
        );
      if(m == null) throw new MissingMethodException();

      try {
        return m.Invoke(this.Target, args);
      } catch(Exception ex) {
        throw ex.InnerException;
      }
    }
  }
}

