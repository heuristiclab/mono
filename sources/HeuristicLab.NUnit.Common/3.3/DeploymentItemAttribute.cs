using System;
using System.IO;

namespace HeuristicLab.NUnit.Common {
  // fake MSTest.DeploymentItem
  [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
  public class DeploymentItemAttribute : Attribute {

    public DeploymentItemAttribute(
      string filename,
      string outdir = null,
      string root = null
    ) {
      string execdir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
      if(outdir == null)
        outdir = execdir;
      if(root == null)
        root = Path.Combine(execdir, "../.."); // bin/Debug/../..

      filename = Path.Combine(Path.GetFullPath(root), filename);
      string newfile = Path.Combine(outdir, Path.GetFileName(filename));
      File.Copy(filename, newfile, true);
      File.SetAttributes(newfile, FileAttributes.Normal);
    }
  }
}

