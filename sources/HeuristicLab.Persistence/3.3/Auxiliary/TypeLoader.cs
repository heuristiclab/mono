﻿#region License Information
/* HeuristicLab
 * Copyright (C) 2002-2012 Heuristic and Evolutionary Algorithms Laboratory (HEAL)
 *
 * This file is part of HeuristicLab.
 *
 * HeuristicLab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HeuristicLab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HeuristicLab. If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Reflection;
using HeuristicLab.Persistence.Core;
using HeuristicLab.Tracing;

namespace HeuristicLab.Persistence.Auxiliary {

  internal class TypeLoader {

    public static Type Load(string typeNameString) {
      try {
        // try to load type normally
        return LoadInternal(typeNameString);
      } catch (PersistenceException) {
        // if that fails, try to load mono type
        string monoTypeNameString = GetMonoType(typeNameString);
        Logger.Info(String.Format(@"Trying to load Mono type ""{0}"" instead of type ""{1}""",
          monoTypeNameString, typeNameString));
        return LoadInternal(monoTypeNameString);
      }
    }

    private static Type LoadInternal(string typeNameString) {
      Type type;
      try {
        type = Type.GetType(typeNameString, true);
        // TODO workaround until [mono bug #580](http://bugzilla.xamarin.com/show_bug.cgi?id=580) is fixed
        if(type.AssemblyQualifiedName != typeNameString)
          throw new TypeLoadException(
            String.Format(
              @"Could not load requested type ""{0}"", loaded ""{1}"" instead.",
              typeNameString, type.AssemblyQualifiedName));
      }
      catch (Exception) {
        Logger.Warn(String.Format(
          "Cannot load type \"{0}\", falling back to partial name", typeNameString));
        type = LoadWithPartialName(typeNameString);
        CheckCompatibility(typeNameString, type);
      }
      return type;
    }

    private static Type LoadWithPartialName(string typeNameString) {
      try {
        TypeName typeName = TypeNameParser.Parse(typeNameString);
#pragma warning disable 0618
        Assembly a = Assembly.LoadWithPartialName(typeName.AssemblyName);
        // the suggested Assembly.Load() method fails to load assemblies outside the GAC
#pragma warning restore 0618
        return a.GetType(typeName.ToString(false, false), true);
      }
      catch (Exception) {
        throw new PersistenceException(String.Format(
          "Could not load type \"{0}\"",
          typeNameString));
      }
    }

    private static void CheckCompatibility(string typeNameString, Type type) {
      try {
        TypeName requestedTypeName = TypeNameParser.Parse(typeNameString);
        TypeName loadedTypeName = TypeNameParser.Parse(type.AssemblyQualifiedName);
        if (!requestedTypeName.IsCompatible(loadedTypeName))
          throw new PersistenceException(String.Format(
            "Serialized type is incompatible with available type: serialized: {0}, loaded: {1}",
            typeNameString,
            type.AssemblyQualifiedName));
        if (requestedTypeName.IsNewerThan(loadedTypeName))
          throw new PersistenceException(String.Format(
            "Serialized type is newer than available type: serialized: {0}, loaded: {1}",
            typeNameString,
            type.AssemblyQualifiedName));
      }
      catch (PersistenceException) {
        throw;
      }
      catch (Exception e) {
        Logger.Warn(String.Format(
          "Could not perform version check requested type was {0} while loaded type is {1}:",
          typeNameString,
          type.AssemblyQualifiedName),
          e);
      }
    }
  
    /// <summary>
    /// Returns the corresponding type for the Mono runtime
    /// </summary>
    /// <returns>
    /// The remapped typeNameString, or the original string if no mapping was found
    /// </returns>
    /// <param name='typeNameString'>
    /// Type name string.
    /// </param>
    private static string GetMonoType(string typeNameString) {
      TypeName typeName = TypeNameParser.Parse(typeNameString);
      
      // map System.RuntimeType to System.MonoType
      if(typeName.Namespace == "System" && typeName.ClassName == "RuntimeType") {
        typeName = TypeNameParser.Parse("System.MonoType, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
      } else
      // map System.Collections.Generic.ObjectEqualityComparer to HeuristicLab.Mono.ObjectEqualityComparer
      // HeuristicLab.Mono.ObjectEqualityComparer`1[[???]], HeuristicLab.Mono, Version=3.3.0.0, Culture=neutral, PublicKeyToken=ba48961d6f65dcec
      if(typeName.Namespace == "System.Collections.Generic" && typeName.ClassName == "ObjectEqualityComparer") {
        typeName.Namespace = "HeuristicLab.Mono";
        typeName.AssemblyName = "HeuristicLab.Mono";
        typeName.AssemblyAttributes.Clear();
        typeName.AssemblyAttributes["Version"] = "3.3.0.0";
        typeName.AssemblyAttributes["Culture"] = "neutral";
        typeName.AssemblyAttributes["PublicKeyToken"] = "ba48961d6f65dcec";
      }

      return typeName.ToString(true, true);
    }
  }
}