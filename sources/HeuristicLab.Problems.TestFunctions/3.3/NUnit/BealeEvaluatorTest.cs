﻿#region License Information
/* HeuristicLab
 * Copyright (C) 2002-2011 Heuristic and Evolutionary Algorithms Laboratory (HEAL)
 *
 * This file is part of HeuristicLab.
 *
 * HeuristicLab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HeuristicLab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HeuristicLab. If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using HeuristicLab.Encodings.RealVectorEncoding;
using HeuristicLab.Problems.TestFunctions;
using NUnit.Framework;
using HeuristicLab.NUnit.Common;

namespace HeuristicLab.Problems.TestFunctions_33.NUnit {


  /// <summary>
  ///This is a test class for BealeEvaluatorTest and is intended
  ///to contain all BealeEvaluatorTest Unit Tests
  ///</summary>
  [TestFixture]
  public class BealeEvaluatorTest {

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[TestFixtureSetUp]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion

    /// <summary>
    ///A test for EvaluateFunction
    ///</summary>
    [Test]
    [DeploymentItem("HeuristicLab.Problems.TestFunctions-3.3.dll")]
    public void BealeEvaluateFunctionTest() {
      BealeEvaluator bealeEvaluator = new BealeEvaluator();
      var target = new Accessor(bealeEvaluator);
      RealVector point = null;
      double expected = bealeEvaluator.BestKnownQuality;
      double actual;
      for (int dimension = bealeEvaluator.MinimumProblemSize;
          dimension <= System.Math.Min(10, bealeEvaluator.MaximumProblemSize);
          dimension++) {
        point = bealeEvaluator.GetBestKnownSolution(dimension);
        actual = (double)target.Call("EvaluateFunction", point);
        Assert.AreEqual(expected, actual);
      }
    }
  }
}
