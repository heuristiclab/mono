﻿#region License Information
/* HeuristicLab
 * Copyright (C) 2002-2011 Heuristic and Evolutionary Algorithms Laboratory (HEAL)
 *
 * This file is part of HeuristicLab.
 *
 * HeuristicLab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HeuristicLab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HeuristicLab. If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using HeuristicLab.Encodings.RealVectorEncoding;
using HeuristicLab.Problems.TestFunctions;
using NUnit.Framework;
using HeuristicLab.NUnit.Common;

namespace HeuristicLab.Problems.TestFunctions_33.NUnit {


  /// <summary>
  ///This is a test class for RosenbrockEvaluatorTest and is intended
  ///to contain all RosenbrockEvaluatorTest Unit Tests
  ///</summary>
  [TestFixture]
  public class RosenbrockEvaluatorTest {

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[TestFixtureSetUp]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion

    /// <summary>
    ///A test for EvaluateFunction
    ///</summary>
    [Test]
    [DeploymentItem("HeuristicLab.Problems.TestFunctions-3.3.dll")]
    public void RosenbrockEvaluateFunctionTest() {
      RosenbrockEvaluator rosenbrockEvaluator = new RosenbrockEvaluator();
      var target = new Accessor(rosenbrockEvaluator);
      RealVector point = null;
      double expected = rosenbrockEvaluator.BestKnownQuality;
      double actual;
      for (int dimension = rosenbrockEvaluator.MinimumProblemSize;
          dimension <= System.Math.Min(10, rosenbrockEvaluator.MaximumProblemSize);
          dimension++) {
        point = rosenbrockEvaluator.GetBestKnownSolution(dimension);
        actual = (double)target.Call("EvaluateFunction", point);
        Assert.AreEqual(expected, actual);
      }
    }
  }
}
