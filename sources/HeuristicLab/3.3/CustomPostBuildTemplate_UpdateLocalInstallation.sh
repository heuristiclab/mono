#!/bin/sh

error() {
	echo "ERROR $1";
	exit ${2:-1};
}

PROJECTDIR="."
SOLUTIONDIR="../.."
OUTDIR="bin/Debug"
TARGET="$HOME/HeuristicLab 3.3/"

cp "$OUTDIR/HeuristicLab 3.3.exe" "$OUTDIR/HeuristicLab 3.3.exe.config" \
	"$OUTDIR/HeuristicLab.PluginInfrastructure-3.3.dll" "$TARGET";

if [ ! -d "$TARGET" ]; then
	mkdir "$TARGET";
fi

IFS=':';
sed '1,3d;s#\\#/#g' "$PROJECTDIR/Files.txt" | \
while read DIR FILE; do
	test -z "$FILE" && continue;
	cp "$SOLUTIONDIR/$DIR/$OUTDIR/$FILE" "$TARGET" #&>/dev/null
done

echo "Platform: $PLATFORM, architecture: $PROC_ARCH"
if [ x"$PLATFORM" = xx86 -o x"$PROC_ARCH" = xx86 ]; then
# TODO
	IFS=':';
	sed '1,3d;s#\\#/#g' "$PROJECTDIR/Files.x86.txt" | \
	while read DIR FILE; do
		test -z "$FILE" && continue;
		cp "$SOLUTIONDIR/$FILE" "$TARGET" #&>/dev/null
	done
elif [ x"$PLATFORM" = xx64 -o x"$PROC_ARCH" = xx64 ]; then
# TODO
	IFS=':';
	sed '1,3d;s#\\#/#g' "$PROJECTDIR/Files.x86.txt" | \
	while read DIR FILE; do
		test -z "$FILE" && continue;
		cp "$SOLUTIONDIR/$FILE" "$TARGET" #&>/dev/null
	done
else
	error "unknown platform and architecture: $PLATFORM $PROC_ARCH";
fi

echo "CustomPostBuild done";
exit 0;

