#!/bin/sh

ROOT=${*:-.}

REV=`git describe --tags --dirty=-d|sed -E 's/^.*-([0-9]+)-.*$/\1/'`
echo creating version $REV from frame files

find $ROOT -name 'AssemblyInfo.frame' |
while read f; do
	sed 's/\$WCREV\$/'$REV'/g' "$f" > "${f%.frame}.cs"
done

find $ROOT -name '*.cs.frame' |
while read f; do
	sed 's/\$WCREV\$/'$REV'/g' "$f" > "${f%.frame}"
done
