#!/bin/sh
# Tries its best to convert MSTest files NUnit tests.
# Uses simple sed magic to accomplish that

if [ $# -lt 1 ]; then
	echo "usage: $0 <directory>"
	exit 1;
fi

DIR=$1

sed -i \
-e 's/^using Microsoft.VisualStudio.TestTools.UnitTesting;/using NUnit.Framework;/;	# use nunit' \
-e 's/^\(namespace HeuristicLab\..*\)\.Tests\? \?{/\1.NUnit {/;	# fix namespace' \
-e 's/\[TestClass\(()\)\?\]/[TestFixture]/;	# mark test classes' \
-e 's/\[TestMethod\(()\)\?\]/[Test]/;	# mark test methods' \
-e 's/\[ClassInitialize\(()\)\?\]/[TestFixtureSetUp]/;	# mark setup methods' \
 "$DIR/"*.cs

